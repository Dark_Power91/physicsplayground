﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PalettaMolla : MonoBehaviour {

	[SerializeField]
	private float m_forceScale;

	void OnCollisionEnter (Collision collision)
	{
		Rigidbody rb = GetComponent<Rigidbody>();
		foreach (ContactPoint contact in collision.contacts)
		{
			if (!contact.otherCollider.CompareTag("Player"))
				continue;
			rb.AddForce(m_forceScale, 0, 0);
		}
	}
}
