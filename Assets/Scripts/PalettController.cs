﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PalettController : MonoBehaviour {

    [SerializeField]
    private float m_forceScale;

    void FixedUpdate()
    {
        bool b = Input.GetButton("Fire2");
        Rigidbody rb = GetComponent<Rigidbody>();

        if(b)
        {
            Vector3 v = new Vector3(-1, 0,0) * m_forceScale;
            rb.AddForce(v);

        }

    }
}
