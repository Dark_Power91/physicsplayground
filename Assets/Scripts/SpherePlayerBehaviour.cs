﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpherePlayerBehaviour : MonoBehaviour {

	[SerializeField]
	private float m_forceScale;

	[SerializeField]
	private bool m_useTorque;

	[SerializeField]
	private bool m_useImpulse;

    [SerializeField]
    private bool m_useMouseClick;


	
	// Update is called once per frame
	void FixedUpdate () {
		float h = Input.GetAxis("Horizontal");
		float v = Input.GetAxis("Vertical");
		Rigidbody rb = GetComponent<Rigidbody>();

		ForceMode fm = m_useImpulse ? ForceMode.Impulse : ForceMode.Force;

        if(m_useMouseClick)
        {
            if (Input.GetButton("Fire1"))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hitInfo = new RaycastHit();
                bool hit = Physics.Raycast(ray,  out hitInfo);
                if(hit)
                {
                    Vector3 delta = (hitInfo.point - transform.position).normalized;
                    h = delta.x;
                    v = delta.z;
                }
             
            }
        }

		if (m_useTorque)
		{
			Vector3 vec = new Vector3(v, 0, h) * m_forceScale;
			rb.AddTorque(vec, fm);
		}
		else
		{
			Vector3 vec = new Vector3(h,0,v) * m_forceScale;
				rb.AddForce(vec,fm);

		}
	   

		

		
	}
}
